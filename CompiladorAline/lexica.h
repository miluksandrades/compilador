int  parenteses[2], colchete[2], chaves[2], aspas[2], aux = 0, countPrincipal = 0;

//Primeiro passo da analise lexica: Checa se os caracteres estao de acordo com o alfabeto aceito pelo compilador
void checaCaracteres(char caractere, int linha)
{
	
	if((int) caractere >= 128 || (int) caractere < 1)
	{
		checaErros((char)caractere,linha, 002);
	}
}

//Segundo passo da analise lexica: Checa se os parenteses, chaves, colchetes, etc estao abrindo e fechando corretamente
void checaAbertoFechado(char caractere, int linha, bool linhaFinal)
{
	if(aux == 0)
	{
		parenteses[0] = 0;
		colchete[0] = 0;
		chaves[0] = 0;
		aspas[0] = 1;
		aux++;
	}
	
	//Checa se a tem a mesma quantidade de delimitadores abrindo e fechando
	if(!linhaFinal)
	{
		if((int)caractere ==  40)
		{
			parenteses[0]++;
			parenteses[1] = linha;
		}
		else if((int)caractere == 41)
		{
			parenteses[0]--;
			parenteses[1] = linha;
		}
		else if((int)caractere == 91)
		{
			colchete[0]++;
			colchete[1] = linha;
		}
		else if((int)caractere == 93)
		{
			colchete[0]--;	
			colchete[1] = linha;
		}
		else if((int)caractere == 123)
		{
			chaves[0]++;
			chaves[1] = linha;
		}
		else if((int)caractere == 125)
		{
			chaves[0]--;
			chaves[1] = linha;
		}
		else if((int)caractere == 34)
		{
			aspas[0] *= -1;
			aspas[1] = linha;
		}
	}
	
	//Retorna erros se delimitadores n�o estiverem abrindo e fechando corretamente
	if(linhaFinal)
	{
		if(parenteses[0] > 0)
			checaErros((char) 40, parenteses[1], 003);
		else if(parenteses[0] < 0)
			checaErros((char) 41, parenteses[1], 004);
			
		if(colchete[0] > 0)
			checaErros((char) 91, colchete[1], 003);
		else if(colchete[0] < 0)
			checaErros((char) 93, colchete[1], 004);
			
		if(chaves[0] > 0)
			checaErros((char) 123, chaves[1], 003);
		else if(chaves[0] < 0)
			checaErros((char) 125, chaves[1], 004);
			
		if(aspas[0] != 1)
			checaErros((char) 34, aspas[1], 003);
	}
		
}

//Terceira parte da an�lise l�xica: Checa se o token em quest�o � uma palavra reservada
void checaToken(char *tok, int linha) 
{
	//p r i n c i p a l
	if(tok[0] == (char) 112 && tok[1] == (char) 114 && tok[2] == (char) 105 && tok[3] == (char) 110 && tok[4] == (char) 99 && tok[5] == (char) 105 && tok[6] == (char) 112 && tok[7] == (char) 97 && tok[8] == (char) 108 && !tok[9])
		checaErros(tok, linha, 8);
	//f u n c a o
	else if(tok[0] == (char) 102 && tok[1] == (char) 117 && tok[2] == (char) 110 && tok[3] == (char) 99 && tok[4] == (char) 97 && tok[5] == (char) 111 && !tok[6])
		checaErros(tok, linha, 8);
	//l e i t u r a
	else if(tok[0] == (char) 108 && tok[1] == (char) 101 && tok[2] == (char) 105 && tok[3] == (char) 116 && tok[4] == (char) 117 && tok[5] == (char) 114 && tok[6] == (char) 97 && !tok[7])
		checaErros(tok, linha, 8);
	//e s c r i t a
	else if(tok[0] == (char) 101 && tok[1] == (char) 115 && tok[2] == (char) 99 && tok[3] == (char) 114 && tok[4] == (char) 105 && tok[5] == (char) 116 && tok[6] == (char) 97 && !tok[7])
		checaErros(tok, linha, 8);
	//s e
	else if(tok[0] == (char) 115 && tok[1] == (char) 101 && !tok[2])
		checaErros(tok, linha, 8);
	//s e n a o
	else if(tok[0] == (char) 115 && tok[1] == (char) 101 && tok[2] == (char) 110 && tok[3] == (char) 97 && tok[4] == (char) 111 && !tok[5])
		checaErros(tok, linha, 8);
	//p a r a	
	else if(tok[0] == (char) 112 && tok[1] == (char) 97 && tok[2] == (char) 114 && tok[3] == (char) 97 && !tok[4])
		checaErros(tok, linha, 8);
	//i n t e i r o
	else if(tok[0] == (char) 105 && tok[1] == (char) 110 && tok[2] == (char) 116 && tok[3] == (char) 101 && tok[4] == (char) 105 && tok[5] == (char) 114 && tok[6] == (char) 111 && !tok[7])
		checaErros(tok, linha, 8);
	//c a r a c t e r e
	else if(tok[0] == (char) 99 && tok[1] == (char) 97 && tok[2] == (char) 114 && tok[3] == (char) 97 && tok[4] == (char) 99 && tok[5] == (char) 116 && tok[6] == (char) 101 && tok[7] == (char) 114 && tok[8] == (char) 101 && !tok[9])
		checaErros(tok, linha, 8);
	//d e c i m a l
	else if(tok[0] == (char) 100 && tok[1] == (char) 101 && tok[2] == (char) 99 && tok[3] == (char) 105 && tok[4] == (char) 109 && tok[5] == (char) 97 && tok[6] == (char) 108 && !tok[7])
		checaErros(tok, linha, 8);
}

//Verifica se a fun��o existe
void validaNomeFuncao(char *tok, int linha)
{
	int identificada = 0;
	//p r i n c i p a l
	if(tok[0] == (char) 112 && tok[1] == (char) 114 && tok[2] == (char) 105 && tok[3] == (char) 110 && tok[4] == (char) 99 && tok[5] == (char) 105 && tok[6] == (char) 112 && tok[7] == (char) 97 && tok[8] == (char) 108 && !tok[9])
	{
		countPrincipal++;
		if(countPrincipal>1)
		{
			checaErros(tok,linha,18);
		}
		identificada = 1;
	}
	//f u n c a o
	else if(tok[0] == (char) 102 && tok[1] == (char) 117 && tok[2] == (char) 110 && tok[3] == (char) 99 && tok[4] == (char) 97 && tok[5] == (char) 111 && !tok[6])
		identificada = 1;
	//l e i t u r a
	else if(tok[0] == (char) 108 && tok[1] == (char) 101 && tok[2] == (char) 105 && tok[3] == (char) 116 && tok[4] == (char) 117 && tok[5] == (char) 114 && tok[6] == (char) 97 && !tok[7])
		identificada = 1;
	//e s c r i t a
	else if(tok[0] == (char) 101 && tok[1] == (char) 115 && tok[2] == (char) 99 && tok[3] == (char) 114 && tok[4] == (char) 105 && tok[5] == (char) 116 && tok[6] == (char) 97 && !tok[7])
		identificada = 1;
	//s e
	else if(tok[0] == (char) 115 && tok[1] == (char) 101 && !tok[2])
		identificada = 1;
	//s e n a o
	else if(tok[0] == (char) 115 && tok[1] == (char) 101 && tok[2] == (char) 110 && tok[3] == (char) 97 && tok[4] == (char) 111 && !tok[5])
		identificada = 1;
	//p a r a	
	else if(tok[0] == (char) 112 && tok[1] == (char) 97 && tok[2] == (char) 114 && tok[3] == (char) 97 && !tok[4])
		identificada = 1;
	//i n t e i r o
	else if(tok[0] == (char) 105 && tok[1] == (char) 110 && tok[2] == (char) 116 && tok[3] == (char) 101 && tok[4] == (char) 105 && tok[5] == (char) 114 && tok[6] == (char) 111 && !tok[7])
		identificada = 1;
	//c a r a c t e r e
	else if(tok[0] == (char) 99 && tok[1] == (char) 97 && tok[2] == (char) 114 && tok[3] == (char) 97 && tok[4] == (char) 99 && tok[5] == (char) 116 && tok[6] == (char) 101 && tok[7] == (char) 114 && tok[8] == (char) 101 && !tok[9])
		identificada = 1;
	//d e c i m a l
	else if(tok[0] == (char) 100 && tok[1] == (char) 101 && tok[2] == (char) 99 && tok[3] == (char) 105 && tok[4] == (char) 109 && tok[5] == (char) 97 && tok[6] == (char) 108 && !tok[7])
		identificada = 1;
	//Verifica que � uma fun��o j� adicionada
	else if(VerificaInseridosFuncao(tok) == 1)
		identificada = 1;
		
	if(identificada != 1)
		checaErros(tok, linha, 15);
}
