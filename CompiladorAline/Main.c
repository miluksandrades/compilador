#include<string.h>
#include<stdlib.h>
#include<locale.h>
#include<stdbool.h>
#include<stdio.h>
#include<conio.h>
#include"lexica.h"
#include"erros.h"
#include"semantica.h"

//Vari�veis globais
FILE *arquivo;
int tamanho = 0;
int linha = 1;

// Criando a tabela de fun��es
typedef struct listaFuncao {
	char *nome;
	struct listaFuncao *proximoF;
} tabelaFuncao;

// Criando a tabela de simbolos
typedef struct listaSimbolo {
	char *nome;
	char *tipo;
	char *valor;
	char *tamanhoVetor;
	struct listaSimbolo *proximo;
} tabelaSimbolo;

// Iniciando funcoes
char* retiraInutil (char* textoInicial);

tabelaFuncao *raizFuncao = NULL, *folhaFuncao = NULL;
tabelaSimbolo *raiz = NULL, *folha = NULL;
tabelaFuncao *inserirTabelaFuncao(char *nome);
tabelaSimbolo *inserirTabelaSimbolo(char *nome, char *tipo, char *valor, char *tamanhoVetor);

char *concatenaString(const char *orig, char c);
char *leCodigo(char *texto);

int VerificaInseridosFuncao(char *nome);

void ImprimirLista();
void VerificaInseridos(char *nome);

int main()
{	
	const size_t tamanhoCodigo = 3000;
	char *codigo = malloc(tamanhoCodigo), *codigoPosLexica = malloc(tamanhoCodigo);
	int aux = 0;
	
	setlocale(LC_ALL, "PORTUGUESE");
	// Atribuindo arquivo
	fopen_s(&arquivo, "Arquivo.txt", "r");

	//Checando se o arquivo existe
	if (arquivo == NULL){
   		checaErros((char)01,linha,000);
	}
	
	while (fgets(codigo, tamanhoCodigo, arquivo) != NULL)
	{
		codigo = retiraInutil(codigo);
		//Copia a string para percorrer o c�digo novamente ap�s a an�lise l�xica
		if(aux == 0)
		{			
			strcpy(codigoPosLexica, codigo);
			aux++;
		}
		else
		strcat(codigoPosLexica, codigo);
	}
	checaAbertoFechado((char)006, linha,true);
	
	//Verifica se existe a func�o principal() no c�digo
	if(!strstr(codigoPosLexica,"principal()"))
	{
		checaErros(codigoPosLexica, 000, 005);
	}
	//Resetando a linha ap�s a an�lise lexica
	linha = 1;
    
	//Segunda verifica��o do c�digo
	codigo = leCodigo(codigoPosLexica);
	ImprimirLista();
	printf("\nBytes Utilizados: %i\n", tamanho);
   	system("pause");
   	exit(0);
		
	
}

//Inserindo vari�veis na tabela de fun��es
tabelaFuncao *inserirTabelaFuncao(char *nome)
{
	if (raizFuncao == NULL)
	{
		raizFuncao = (tabelaFuncao *) malloc(sizeof(tabelaFuncao));
		folhaFuncao = raizFuncao;
	}
	else
	{
		folhaFuncao->proximoF = (tabelaFuncao *) malloc(sizeof(tabelaSimbolo));
		folhaFuncao = folhaFuncao->proximoF;
	}
	folhaFuncao->nome = (char*)malloc(strlen(nome));
	strncpy(folhaFuncao->nome, nome, strlen(nome)+1);
	
	tamanho = tamanho + sizeof(folhaFuncao->nome);
}

//Inserindo variaveis na tabela de simbolos
tabelaSimbolo *inserirTabelaSimbolo(char *nome, char *tipo, char *valor, char *tamanhoVetor)
{	
	if (raiz == NULL)
	{
		raiz = (tabelaSimbolo *) malloc(sizeof(tabelaSimbolo));	  
		folha = raiz;
	}
		
	else 
	{
		folha->proximo = (tabelaSimbolo *) malloc(sizeof(tabelaSimbolo));
		folha = folha->proximo;
	}
	folha->nome = (char *)malloc(strlen(nome));
    strncpy(folha->nome, nome, strlen(nome)+1);
    
    folha->tipo = (char *)malloc(strlen(tipo));
    strncpy(folha->tipo, tipo, strlen(tipo)+1);
    
    folha->valor = (char *)malloc(strlen(valor));
    strncpy(folha->valor, valor, strlen(valor)+1);
    
    folha->tamanhoVetor = (char *)malloc(strlen(tamanhoVetor));
    strncpy(folha->tamanhoVetor, tamanhoVetor, strlen(tamanhoVetor)+1);
    folha->proximo = NULL;
    
    //Adicionando quantidade de bytes
    tamanho = tamanho + sizeof(folha->nome);
    tamanho = tamanho + sizeof(folha->valor);
    tamanho = tamanho + sizeof(folha->tipo);
    tamanho = tamanho + sizeof(folha->tamanhoVetor);
    tamanho = tamanho + sizeof(folha);
	
	
    //printf("\n -----------------------\n Tipo: %s \n Nome: %s \n Tamanho do Vetor: %s \n Valor: %s \n Inseridos na tabela com sucesso!\n ----------------------- \n",tipo, nome, tamanhoVetor, valor);
    return folha;
}

//Remove caracteres inuteis, como espaco, quebra de linha, etc
char* retiraInutil (char *textoInicial)
{
    int i,j;
    bool aspas = 0;
    char *textoFinal=textoInicial;
    
    
    for (i = 0, j = 0; i<strlen(textoInicial); i++,j++)
    {
    	//Verifica se ha espacos entre caracteres que nao podem haver espacos    	
    	if((textoInicial[i] == 62 || textoInicial[i] == 60) && (textoInicial[i+1] == 255|| textoInicial[i+1] == 13 || textoInicial[i+1] ==  32 ) && (textoInicial[i+2] == 61 || textoInicial[i+3] == 61 || textoInicial[i+4] == 61 )) 
    	{
    		checaErros((char *)001,linha, 001);
		}
		
		//Ignora os espacos entre aspas	    
    	if(textoInicial[i] == (char) 34)
        {
        	aspas ^= 1;
		}
		
		if(textoInicial[i] == (char) 10)
		{
			linha++;
		}
 		checaCaracteres(textoInicial[i], linha);
 		checaAbertoFechado(textoInicial[i], linha, false);
 		
		
		//Retira espacos, quebras de linha e tabs
        if ((textoInicial[i] != (char) 32 && textoInicial[i] != (char) 9 /*&& textoInicial [i] != (char) 10*/) || aspas)
		{   
            textoFinal[j]=textoInicial[i];
		}      
        else
        {
            j--;
		}                   
    }
    textoFinal[j]=0;    
    //printf("%s",textoFinal);
    
    return textoFinal;
}
//Adiciona um char � uma string
char *concatenaString(const char *orig, char c)
{
    size_t sz = strlen(orig);
    char *str = malloc(sz + 2);
    strcpy(str, orig);
    str[sz] = c;
    str[sz + 1] = '\0';
    return str;
}

// L� o c�digo e verifica vari�veis e fun��es
char *leCodigo(char *texto)
{
	int i, j, aspas = 1, countPonto = 0;	
    char *nome = (char *)malloc(sizeof(texto[i])), *tipo = (char *)malloc(sizeof(texto[i])), *valor = (char *)malloc(sizeof(texto[i])), *tamanhoVetor = (char *)malloc(sizeof(texto[i]));
	nome = "";
	tipo = "";
	valor = "";
	tamanhoVetor = "";
	
	// TRATAMENTO DE VARI�VEIS
    for (i = 0; i<strlen(texto); i++)
    {
    	if(texto[i] == (char) 10)
    		linha++;
    	//i n t e i r o
    	if(texto[i] == (char) 105 && texto[i+1] == (char) 110 && texto[i+2] == (char) 116 && texto[i+3] == (char) 101 && texto[i+4] == (char) 105 && texto[i+5] == (char) 114 && texto[i+6] == (char) 111)
    	{
    		
			tipo = "inteiro";
			i = i+7;
			j = i;
    		
			//&	
			if(texto[i] != (char) 38)
			{
				checaErros((char*)tipo, linha, 006);				
			}
			while(texto[j] != (char) 59)
			{					
				//&
				if(texto[j] == (char) 44 && texto[j+1] != (char) 38)
				{
					checaErros((char*)tipo, linha, 006);				
				}
				j++;
			}
			
		}
		//c a r a c t e r e
    	if(texto[i] == (char) 99 && texto[i+1] == (char) 97 && texto[i+2] == (char) 114 && texto[i+3] == (char) 97 && texto[i+4] == (char) 99 && texto[i+5] == (char) 116 && texto[i+6] == (char) 101 && texto[i+7] == (char) 114 && texto[i+8] == (char) 101)
    	{
			tipo = "caractere";
			i = i+9;
			j = i;
    			
			//&
			if(texto[i] != (char) 38)
			{
				checaErros((char*)tipo, linha, 006);				
			}
			while(texto[j] != (char) 59)
			{					
				//&
				if(texto[j] == (char) 44 && texto[j+1] != (char) 38)
				{
					checaErros((char*)tipo, linha, 006);				
				}
				j++;
			}
			
		}
		//d e c i m a l
    	if(texto[i] == (char) 100 && texto[i+1] == (char) 101 && texto[i+2] == (char) 99 && texto[i+3] == (char) 105 && texto[i+4] == (char) 109 && texto[i+5] == (char) 97 && texto[i+6] == (char) 108)
    	{
			tipo = "decimal";
			i = i+7;
			j = i;
    			
			//&
			if(texto[i] != (char) 38)
			{
				checaErros((char*)tipo, linha, 006);				
			}
			while(texto[j] != (char) 59)
			{					
				//&
				if(texto[j] == (char) 44 && texto[j+1] != (char) 38)
				{
					checaErros((char*)tipo, linha, 006);				
				}
				j++;
			}
			
		}
		
	
		//&
		if(texto[i] == (char) 38)
		{
			//if(texto[i-1] != (char) 44)
			//	checaErros(",", linha, 21);
			
			if(!(tipo == "caractere" || tipo == "inteiro" || tipo == "decimal"))
			{
				checaErros(tipo, linha, 20);
			}
			i++;
			// Se o primeiro s�mbolo da vari�vel for diferente a...z
			if((int) texto[i] < 97 || (int) texto[i] > 122)
			{
				checaErros((char*)"", linha, 007);
			}
			// Enquanto n�o encontra os delimitadores [ , ; = ir� adicionar � vari�vel
			while(texto[i] != (char) 91 && texto[i] != (char) 44 && texto[i] != (char) 59 && texto[i] != (char) 61)
			{
				//Enter
				if(texto[i] == (char) 10 && texto[i-1] != (char) 59)
				{	
					checaErros(";", linha, 14);
				}
				// Se o valor estiver entre a...z, A...Z ou 0...9
				if((texto[i] >= (char)97 && !texto[i] <= (char) 122) || (texto[i] >= (char) 065 && texto[i] <= (char) 90) || (texto[i] >= (char) 48  && texto[i] <= (char) 057))
					nome = concatenaString(nome, texto[i]);
				else
					checaErros(texto[i], linha, 10);
				j++;
				i++;
			}
			//Adicionando Vetor
			if(texto[i] == (char) 91)
			{
				i++;
				while(texto[i] != (char) 93)
				{
					
					tamanhoVetor = concatenaString(tamanhoVetor, texto[i]);
					j++;
					i++;
				}
				i++;
			}
			else
			{
				tamanhoVetor = "NULL";
			}
			//Checa se recebe algum valor
			if(texto[i] == (char) 61)
			{
				i++;
				//Se for aspas, ir� ignorar
				if(texto[i] == (char) 34){
					if(strcmp(tipo,"caractere")!=0)
						checaErros(tipo,linha,12);
					aspas *= -1;
					i++;
				}
				// Enquanto n�o encontra os delimitadores [ , ( ; ir� adicionar � vari�vel
				while((texto[i] != (char) 91 && texto[i] != (char) 44 && texto[i] != (char) 59 && texto[i] != (char) 61) || aspas == -1)
				{
					
					
					//Se for aspas, ir� ignorar
					if(texto[i] == (char) 34)
						aspas *= -1;
					if(texto[i] != (char) 34)
						valor = concatenaString(valor, texto[i]);
							
					//Enter
					if(texto[i] == (char) 10 && texto[i-1] != (char) 59)
					{	
						checaErros(";", linha, 14);
					}	
					
					//Se for diferente de n�mero
					if(!(texto[i] >= (char) 48 && texto[i] <= (char) 57) && strcmp(tipo,"inteiro")==0)
						checaErros(tipo,linha,12);
						
					//Se for diferente de um n�mero ou ponto
					if(!( (texto[i] >= (char) 48 && texto[i] <= (char) 57) || (texto[i] == (char) 46) ) && strcmp(tipo,"decimal")==0)
					{
						checaErros(tipo,linha,12);
						
					}
					//Caso o programador coloque mais de um ponto em "decimal"
					else if(strcmp(tipo,"decimal")==0 && texto[i] == (char) 46)
					{
						countPonto++;
					}
					
					if(countPonto > 1)
					{
						checaErros(valor, linha, 13);
					}
					
					j++;
					i++;
				}
				countPonto = 0;
			}
			else
			{
				valor = "NULL";
			}
			
			//Verifica se a quantidade de caracteres � igual ou menor que o tamanho do vetor
			if(tipo == "caractere" && tamanhoVetor != "NULL" && atoi(tamanhoVetor) < strlen(valor))
				checaErros(valor, linha, 9);
			checaToken(nome, linha);
			VerificaInseridos(nome);	
			inserirTabelaSimbolo(nome,tipo,valor,tamanhoVetor);
			
			//reseta os valores
			if(texto[i] == (char) 59)
			{
				tipo = "";
			}
			nome = "";
			valor = "";
			tamanhoVetor = "";		
	    				
		}
		
		//Trabalho Adiantado
    	//p
    	if(texto[i] == (char) 112)
    	{
    		//i n c i p a l
    		if(texto[i+1] == (char) 114 && texto[i+2] ==  (char) 105 && texto[i+3] ==  (char) 110 && texto[i+4] ==  (char) 99 && texto[i+5] ==  (char) 105 && texto[i+6] ==  (char) 112  && texto[i+7] ==  (char) 97 && texto[i+8] ==  (char) 108)
    		{
    			i = i+8;
			}
		}
		
		// FUN��ES
		//f u n c a o
		if(texto[i] == (char) 102 && texto[i+1] == (char) 117 && texto[i+2] == (char) 110 && texto[i+3] == (char) 99 && texto[i+4] == (char) 97 && texto[i+5] == (char) 111 )
    	{
			i = i+6;
			if(texto[i] != (char) 102)
			{
				checaErros("",linha, 19);
			}
			while(texto[i] != (char) 40)
			{
				// Se o valor estiver entre a...z, A...Z ou 0...9
				if((texto[i] >= (char)97 && !texto[i] <= (char) 122) || (texto[i] >= (char) 065 && texto[i] <= (char) 90) || (texto[i] >= (char) 48  && texto[i] <= (char) 057))
					nome = concatenaString(nome, texto[i]);
				else
					checaErros(texto[i], linha, 10);
				i++;
			}
			inserirTabelaFuncao(nome);
			nome = "";
				
		}
		
		//Checa se fun��es est�o sendo usadas corretamente
		// )
		if(texto[i] == (char) 41)
		{
			j = i-1;
			// (
			while(texto[j] != (char) 40)
			{
				j--;
			}
			//Enquanto n�o encontra os delimitadores = . ; e enter
			while(!(texto[j-1] == (char) 61 || texto[j-1] == (char) 46 || texto[j-1] == (char) 59 || texto[j-1] == (char) 10) && j > 0)
			{
				j--;
			}
			
			//f u n c a o
			if(texto[j] == (char) 102 && texto[j+1] == (char) 117 && texto[j+2] == (char) 110 && texto[j+3] == (char) 99 && texto[j+4] == (char) 97 && texto[j+5] == (char) 111 && texto[j+6] == (char) 102)
			{
				j = j+6;
			}
			
			//Verificando se a fun��o existe;
			while(texto[j] != (char) 40)
			{
				nome = concatenaString(nome, texto[j]);
				j++;
			}
			
			//Validando Fun��o "para"
			if(strcmp(nome,"para") == 0)
			{
				while(texto[j] != (char) 41)
				{
					if(texto[j] == (char) 59)
						countPonto++;
					j++;
				}
				if(countPonto < 2)
					checaErros(nome, linha, 16);
				else if(countPonto > 2)
					checaErros(nome, linha, 17);
			}
			while(texto[j] != (char) 10)
			{
				j++;
			}
			if(texto[j-1] != (char) 59 && strcmp(nome,"principal") != 0 && nome[0] != (char)102)			
				checaErros(";", linha, 14);
			validaNomeFuncao(nome,linha);
			nome="";
		}
		
	}
	
	//Para poder liberar as vari�veis
	nome = concatenaString(nome, texto[i]);
	valor = concatenaString(valor, texto[i]);
	tamanhoVetor = concatenaString(tamanhoVetor, texto[i]);
	tipo = concatenaString(tipo, texto[i]);
	
	free(valor);
	free(nome);
	free(tamanhoVetor);
	free(tipo);
}


//Verifica se h� uma vari�vel com mesmo nome
int VerificaInseridosFuncao(char *nome)
{
	tabelaFuncao *tb = (tabelaFuncao *) malloc(sizeof(tabelaFuncao));
	int i = 1;
	tb = raizFuncao;
	
	while(tb != NULL && tb->nome != NULL)
	{	
		if(strcmp(nome,tb->nome) == 0)
		{			
			return 1;
		}
		else
		{
			tb = tb->proximoF;
		}
	}
	return 0;
	 
	free(tb);
}

void ImprimirLista()
{
    	
    tabelaSimbolo *aux;
    int i=1;
    
    aux = raiz;
      
	while (aux != NULL) 
	{
       printf("\n -----------------------\n Tipo: %s \n Nome: %s \n Tamanho do Vetor: %s \n Valor: %s\n ----------------------- \n",aux->tipo, aux->nome, aux->tamanhoVetor, aux->valor);
       i++;
       aux = aux->proximo; 
    }
    
    free(aux);
}
//Verifica se h� uma vari�vel com mesmo nome
void VerificaInseridos(char *nome)
{
	tabelaSimbolo *tb = (tabelaSimbolo *) malloc(sizeof(tabelaSimbolo));
	int i = 1;
	tb = raiz;
	
	while(tb != NULL && tb->nome != NULL)
	{	
		if(strcmp(nome,tb->nome) == 0)
		{
			checaErros(nome, linha, 11);
		}
		else
		{
			tb = tb->proximo;
		}
	}
	 
	free(tb);
}

