//Checa os erros do codigo
void checaErros(char *erro, int linha, int numero)
{	
	if ((int) numero == 000){
   		printf("\nErro n�mero %i: Arquivo de nome %s n�o existe.\n", numero, "Arquivo.txt");
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 001)
	{		
   		printf("\nErro n�mero %i: N�o pode haver espa�os entre \"<\" e \"=\" na linha %i\n", numero, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 002)
	{		
   		printf("\nErro n�mero %i: Caractere \"%c\" n�o identificado na linha %i\n", numero, erro, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 003)
	{		
   		printf("\nErro n�mero %i: Caractere \"%c\" � aberto, mas n�o tem fechamento na linha %i\n", numero, erro, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 004)
	{		
   		printf("\nErro n�mero %i: Caractere \"%c\" tem fechamento, mas n�o � aberto na linha %i\n", numero, erro, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 005)
	{		
   		printf("\nErro n�mero %i: Fun��o principal() n�o encontrada.\n", numero);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 006)
	{		
   		printf("\nErro n�mero %i: Espera-se um caractere de \"&\"ap�s %s\n", numero, erro);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 007)
	{		
   		printf("\nErro n�mero %i: Espera-se que o nome da vari�vel comece com \"a...z\".\n", numero);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 8)
	{				
   		printf("\nErro n�mero %i: \"%s\" � uma palavra reservada, n�o � poss�vel criar uma vari�vel com este nome.\n", numero, erro);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 9)
	{				
   		printf("\nErro n�mero %i: A quantidade de caracteres de \"%s\" excede o tamanho do vetor.\n", numero, erro);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 10)
	{				
   		printf("\nErro n�mero %i: Nome de caracteres pode conter apenas a...z, A...Z, 0...9 na linha %i\n", numero, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 11)
	{				
   		printf("\nErro n�mero %i: Vari�vel com nome \"%s\" na linha %i j� foi declarada anteriormente.\n", numero, erro, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 12)
	{				
   		printf("\nErro n�mero %i: Vari�vel s� pode receber tipo \"%s\" na linha %i.\n", numero, erro, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 13)
	{				
   		printf("\nErro n�mero %i: Voc� tentou adiconar mais de um ponto na vari�vel decimal \"%s\" na linha %i.\n", numero, erro, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 14)
	{				
   		printf("\nErro n�mero %i: � esperado um \"%s\" no final da linha %i.\n", numero, erro, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 15)
	{				
   		printf("\nErro n�mero %i: Fun��o \"%s()\" N�o identificada na linha %i.\n", numero, erro, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 16)
	{				
   		printf("\nErro n�mero %i: Poucos argumentos na fun��o para \"%s()\" na linha %i.\n", numero, erro, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 17)
	{				
   		printf("\nErro n�mero %i: Muitos argumentos na fun��o para \"%s()\" na linha %i.\n", numero, erro, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 18)
	{				
   		printf("\nErro n�mero %i: Fun��o  \"%s()\" j� foi declarada anteriormente. Erro na linha %i.\n", numero, erro, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 19)
	{				
   		printf("\nErro n�mero %i: � esperado um  \"f\" ap�s declara��o \"funcao\" na linha %i.\n", numero, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 20)
	{				
   		printf("\nErro n�mero %i: Tipo de vari�vel n�o existe. Erro na linha %i.\n", numero, linha);
   		system("pause");
   		exit(0);
	}
	else if ((int) numero == 21)
	{				
   		printf("\nErro n�mero %i: Espera-se um \",\" antes de \"&\". Erro na linha %i.\n", numero, linha);
   		system("pause");
   		exit(0);
	}
}
